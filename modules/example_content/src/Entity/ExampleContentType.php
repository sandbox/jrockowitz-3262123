<?php

namespace Drupal\example_content\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Example Content type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "example_content_type",
 *   label = @Translation("Example Content type"),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\example_content\Form\ExampleContentTypeForm",
 *       "edit" = "Drupal\example_content\Form\ExampleContentTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\example_content\ExampleContentTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer example content types",
 *   bundle_of = "example_content",
 *   config_prefix = "example_content_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/example_content_types/add",
 *     "edit-form" = "/admin/structure/example_content_types/manage/{example_content_type}",
 *     "delete-form" = "/admin/structure/example_content_types/manage/{example_content_type}/delete",
 *     "collection" = "/admin/structure/example_content_types"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   }
 * )
 */
class ExampleContentType extends ConfigEntityBundleBase {

  /**
   * The machine name of this example content type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the example content type.
   *
   * @var string
   */
  protected $label;

}
