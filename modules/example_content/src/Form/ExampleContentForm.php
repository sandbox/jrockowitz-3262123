<?php

namespace Drupal\example_content\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the example content entity edit forms.
 */
class ExampleContentForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New example content %label has been created.', $message_arguments));
      $this->logger('example_content')->notice('Created new example content %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The example content %label has been updated.', $message_arguments));
      $this->logger('example_content')->notice('Updated new example content %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.example_content.canonical', ['example_content' => $entity->id()]);
  }

}
