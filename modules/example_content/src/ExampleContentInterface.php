<?php

namespace Drupal\example_content;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining an example content entity type.
 */
interface ExampleContentInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the example content title.
   *
   * @return string
   *   Title of the example content.
   */
  public function getTitle();

  /**
   * Sets the example content title.
   *
   * @param string $title
   *   The example content title.
   *
   * @return \Drupal\example_content\ExampleContentInterface
   *   The called example content entity.
   */
  public function setTitle($title);

  /**
   * Gets the example content creation timestamp.
   *
   * @return int
   *   Creation timestamp of the example content.
   */
  public function getCreatedTime();

  /**
   * Sets the example content creation timestamp.
   *
   * @param int $timestamp
   *   The example content creation timestamp.
   *
   * @return \Drupal\example_content\ExampleContentInterface
   *   The called example content entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the example content status.
   *
   * @return bool
   *   TRUE if the example content is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the example content status.
   *
   * @param bool $status
   *   TRUE to enable this example content, FALSE to disable.
   *
   * @return \Drupal\example_content\ExampleContentInterface
   *   The called example content entity.
   */
  public function setStatus($status);

}
