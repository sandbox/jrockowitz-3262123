<?php

namespace Drupal\example_config;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an example config entity type.
 */
interface ExampleConfigInterface extends ConfigEntityInterface {

}
