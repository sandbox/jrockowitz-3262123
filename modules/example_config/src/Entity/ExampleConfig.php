<?php

namespace Drupal\example_config\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\example_config\ExampleConfigInterface;

/**
 * Defines the example config entity type.
 *
 * @ConfigEntityType(
 *   id = "example_config",
 *   label = @Translation("Example Config"),
 *   label_collection = @Translation("Example Configs"),
 *   label_singular = @Translation("example config"),
 *   label_plural = @Translation("example configs"),
 *   label_count = @PluralTranslation(
 *     singular = "@count example config",
 *     plural = "@count example configs",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\example_config\ExampleConfigListBuilder",
 *     "form" = {
 *       "add" = "Drupal\example_config\Form\ExampleConfigForm",
 *       "edit" = "Drupal\example_config\Form\ExampleConfigForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     }
 *   },
 *   config_prefix = "example_config",
 *   admin_permission = "administer example_config",
 *   links = {
 *     "collection" = "/admin/structure/example-config",
 *     "add-form" = "/admin/structure/example-config/add",
 *     "edit-form" = "/admin/structure/example-config/{example_config}",
 *     "delete-form" = "/admin/structure/example-config/{example_config}/delete"
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description"
 *   }
 * )
 */
class ExampleConfig extends ConfigEntityBase implements ExampleConfigInterface {

  /**
   * The example config ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The example config label.
   *
   * @var string
   */
  protected $label;

  /**
   * The example config status.
   *
   * @var bool
   */
  protected $status;

  /**
   * The example_config description.
   *
   * @var string
   */
  protected $description;

}
