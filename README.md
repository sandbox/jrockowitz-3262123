Drupal Code Generator Example
-----------------------------

@see <https://www.drupal.org/sandbox/jrockowitz/3262123>

## Background

There are many ways to learn Drupal, but the reality is Drupal is very challenging to master and has somewhat limited documentation. Besides using books and training videos, it helps to understand what developer-centric resources are available and how to use them to learn new concepts or reinforce existing knowledge.

A trifecta of developer-centric tools, API documentation, examples, and code generation should provide developers with the best way to learn Drupal and reinforce existing knowledge.

I decided to spend 2-3 hours over a week creating an example module using the Drupal Code Generator library, which is now included in Drush.

## Goals

The goals of the Drupal Code Generator Example module are to

- Create a "kitchen sink" example module generated using Drupal Code Generator.

- Understand the scaffolding provided by the Drupal Code Generator.

- Provide a tool/overview for developers to understand the architecture of a Drupal module.

## Notes

- Generated examples are not always working code.

- Namespaces and dependencies are occasionally missing.

- Generated namespaces can be a little tricky.

- Plugins and services have varying namespaces.

- Generating views plugins requires an understanding of the Views API.

- Sub-modules are created for related functionality, including content entities, config entities, and fields.

- Only module-related code generators are being tested and reviewed.

## Recommendations

- Always double-check the generated service name patterns against core and contrib modules.

- If you are an experienced Drupal developer, you should clean up file names and namespaces as needed.

- If you are an in-experience Drupal developer, be careful when tweaking file names and namespaces.

- Be aware of deprecated code warnings.

- Deprecations should be fixed as needed.

### General

- Drupal Code Generator with the Example modules and Googling is the best way to learn how to build a module.

- Use Drupal Code Generator to build a working code scaffolding and then examine how similar code is defined in Drupal core and the Examples module.

- To learn how to build a module from a code first perspective, you should use Drupal's API documentation, the Examples module, and Drupal Code Generator.

- Drupal's documentation with examples and drush's code generation creates a recipe that provides different ways to understand how things are configured and work within a Drupal module.

### Naming conventions

Below are some observations about the namespace recommendations and tweaks that may be needed when using Drupal Code Generator.

Routes and links

- Prefix routes with the module's namespace.

- Links and tasks should minor route names.

Plugins

- Prefix most plugins with the module's class namespace.

- Look for examples of plugin naming conventions in core.

- Use the module's namespace for all plugin annotation ids.

- If a plugin is module specific and only one instance is expected the plugin's id and name can be the same the module's namespace.

Services

- Prefix all services with the module's namespace.

- Some plugins and services should being with the plugin namespace and then suffixed with the module's namespace.

## References

- [Drupal Code Generator](https://github.com/Chi-teck/drupal-code-generator)
  _Drupal Code Generator is maintained by Ivan Chi-teck. Drupal Code Generator
  integrated in the Drush via the `drush generate` command._

- [Examples for Developers](https://www.drupal.org/project/examples)
  _A community maintained module that provide high-quality, well-documented
  API examples for a broad range of Drupal core functionality._

- [Drupal API Reference](https://api.drupal.org/api/drupal)
  _An API reference for Drupal, generated from comments embedded in the
  source code._

- [Drupal API Documentation](https://www.drupal.org/docs/drupal-apis)
  _Community created documentation focused an overview and details on all of
  Drupal's APIs._

- [Using Code Generators in Drupal 8](https://www.webwash.net/using-code-generators-in-drupal/)
  _Ivan Zugec walks-thru three code generators available for Drupal._

## Table of contents.

### _global:

composer
- composer.json

module-info
- drupal_code_generator_example.info.yml

install-file
- drupal_code_generator_example.install

hook
- drupal_code_generator_example.module

javascript
- js/drupal-code-generator-example.js

controller
- drupal_code_generator_example.routing.yml
- src/Controller/DrupalCodeGeneratorExampleController.php

render-element
- src/Element/Entity.php

template
- drupal_code_generator_example.module
- templates/drupal_code_generator_example.html.twig

layout
- drupal_code_generator_example.layouts.yml
- drupal_code_generator_example.libraries.yml
- layouts/drupal_code_generator_example/drupal-code-generator-example.css
- layouts/drupal_code_generator_example/drupal-code-generator-example.html.twig
- layouts/drupal_code_generator_example/drupal-code-generator-example.js

field
- modules/example_field/config/schema/example_field.schema.yml
- modules/example_field/css/example-field-widget.css
- modules/example_field/example_field.libraries.yml
- modules/example_field/src/Plugin/Field/FieldFormatter/ExampleDefaultFormatter.php
- modules/example_field/src/Plugin/Field/FieldFormatter/ExampleKeyValueFormatter.php
- modules/example_field/src/Plugin/Field/FieldFormatter/ExampleTableFormatter.php
- modules/example_field/src/Plugin/Field/FieldType/ExampleItem.php
- modules/example_field/src/Plugin/Field/FieldWidget/ExampleWidget.php

### drush:

drush-command-file
- composer.json
- drush.services.yml
- src/Commands/DrupalCodeGeneratorExampleCommands.php

### module:

module-content-entity
- modules/example_content/config/optional/rest.resource.entity.example_content.yml
- modules/example_content/config/schema/example_content.entity_type.schema.yml
- modules/example_content/example_content.info.yml
- modules/example_content/example_content.links.action.yml
- modules/example_content/example_content.links.menu.yml
- modules/example_content/example_content.links.task.yml
- modules/example_content/example_content.module
- modules/example_content/example_content.permissions.yml
- modules/example_content/src/Entity/ExampleContent.php
- modules/example_content/src/Entity/ExampleContentType.php
- modules/example_content/src/ExampleContentAccessControlHandler.php
- modules/example_content/src/ExampleContentInterface.php
- modules/example_content/src/ExampleContentListBuilder.php
- modules/example_content/src/ExampleContentTypeListBuilder.php
- modules/example_content/src/Form/ExampleContentForm.php
- modules/example_content/src/Form/ExampleContentTypeForm.php
- modules/example_content/templates/example-content.html.twig

module-configuration-entity
- modules/example_config/config/schema/example_config.schema.yml
- modules/example_config/example_config.info.yml
- modules/example_config/example_config.links.action.yml
- modules/example_config/example_config.links.menu.yml
- modules/example_config/example_config.permissions.yml
- modules/example_config/example_config.routing.yml
- modules/example_config/src/Entity/ExampleConfig.php
- modules/example_config/src/ExampleConfigInterface.php
- modules/example_config/src/ExampleConfigListBuilder.php
- modules/example_config/src/Form/ExampleConfigForm.php

### yml:

module-libraries
- drupal_code_generator_example.libraries.yml

permissions
- drupal_code_generator_example.permissions.yml

task-links
- drupal_code_generator_example.links.task.yml

### form:

form-config
- drupal_code_generator_example.links.menu.yml
- drupal_code_generator_example.routing.yml
- src/Form/SettingsForm.php

form-confirm
- drupal_code_generator_example.routing.yml
- src/Form/ExampleConfirmForm.php

form-simple
- drupal_code_generator_example.routing.yml
- src/Form/ExampleForm.php

### plugin:

plugin-action
- config/schema/drupal_code_generator_example.schema.yml
- src/Plugin/Action/UpdateNodeTitleAction.php

plugin-block
- config/schema/drupal_code_generator_example.schema.yml
- src/Plugin/Block/DrupalCodeGeneratorExampleBlock.php

plugin-ckeditor
- js/plugins/drupal-code-generator-example/dialogs/drupal-code-generator-example.js
- js/plugins/drupal-code-generator-example/icons/drupal-code-generator-example.png
- js/plugins/drupal-code-generator-example/plugin.js
- src/Plugin/CKEditorPlugin/DrupalCodeGeneratorExampleCKEditor.php

plugin-condition
- config/schema/drupal_code_generator_example.schema.yml
- src/Plugin/Condition/DrupalCodeGeneratorExampleCondition.php

plugin-constraint
- src/Plugin/Validation/Constraint/DrupalCodeGeneratorExampleConstraint.php
- src/Plugin/Validation/Constraint/DrupalCodeGeneratorExampleConstraintValidator.php

plugin-entity-reference-selection
- config/schema/drupal_code_generator_example.schema.yml
- src/Plugin/EntityReferenceSelection/DrupalCodeGeneratorExampleNodeSelection.php

plugin-filter
- config/schema/drupal_code_generator_example.schema.yml
- src/Plugin/Filter/DrupalCodeGeneratorExampleFilter.php

plugin-menu-link
- src/Plugin/Menu/DrupalCodeGeneratorExampleMenuLink.php

plugin-queue-worker
- src/Plugin/QueueWorker/DrupalCodeGeneratorExampleQueueWorker.php

plugin-rest-resource
- src/Plugin/rest/resource/DrupalCodeGeneratorExampleResource.php

plugin-manager
- drupal_code_generator_example.services.yml
- src/Annotation/DrupalCodeGeneratorExample.php
- src/DrupalCodeGeneratorExampleInterface.php
- src/DrupalCodeGeneratorExamplePluginBase.php
- src/DrupalCodeGeneratorExamplePluginManager.php
- src/Plugin/DrupalCodeGeneratorExample/Foo.php

### service:

service-access-checker
- drupal_code_generator_example.services.yml
- src/Access/DrupalCodeGeneratorExampleAccessChecker.php

service-breadcrumb-builder
- drupal_code_generator_example.services.yml
- src/DrupalCodeGeneratorExampleBreadcrumbBuilder.php

service-cache-context
- drupal_code_generator_example.services.yml
- src/Cache/Context/DrupalCodeGeneratorExampleCacheContext.php

service-custom
- drupal_code_generator_example.services.yml
- src/DrupalCodeGeneratorExampleManager.php

service-event-subscriber
- drupal_code_generator_example.services.yml
- src/EventSubscriber/DrupalCodeGeneratorEventSubscriber.php

service-logger
- drupal_code_generator_example.services.yml
- src/Logger/DrupalCodeGeneratorFileLog.php

service-middleware
- drupal_code_generator_example.services.yml
- src/DrupalCodeGeneratorExampleMiddleware.php

service-param-converter
- drupal_code_generator_example.services.yml
- src/DruplalCodeGeneratorExampleParamConverter.php

service-path-processor
- drupal_code_generator_example.services.yml
- src/PathProcessor/DrupalCodeGeneratorExamplePathProcessor.php

service-provider
- src/DrupalCodeGeneratorExampleServiceProvider.php

service-request-policy
- drupal_code_generator_example.services.yml
- src/PageCache/DrupalCodeGeneratorExampleRequestPolicy.php

service-response-policy
- drupal_code_generator_example.services.yml
- src/PageCache/DrupalCodeGeneratorResponsePolicy.php

service-route-subscriber
- drupal_code_generator_example.services.yml
- src/EventSubscriber/DrupalCodeGeneratorExampleRouteSubscriber.php

service-theme-negotiator
- drupal_code_generator_example.services.yml
- src/Theme/DrupalCodeGeneratorExampleThemeNegotiator.php

service-twig-extension
- drupal_code_generator_example.services.yml
- src/DrupalCodeGeneratorExampleTwigExtension.php

service-uninstall-validator
- drupal_code_generator_example.services.yml
- src/DrupalCodeGeneratorExampleUninstallValidator.php

### test:

test-browser
- tests/src/Functional/DrupalCodeGeneratorExampleTest.php

test-kernel
- tests/src/Kernel/DrupalCodeGeneratorExampleTest.php

test-nightwatch
- tests/src/Nightwatch/drupalCodeGeneratorExampleTest.js

test-unit
- tests/src/Unit/DrupalCodeGeneratorExampleTest.php

test-webdriver
- tests/src/FunctionalJavascript/DrupalCodeGeneratorExampleTest.php

## Commands

### Building an Example Block (example_block) module.

```
# Go to you drupal projects root
cd ~/Sites/drupal_webform

# Generate the Example Block module.
drush generate module-info

# Go to the Example Block module directory.
cd web/modules/custom/example_block

# Create a config  form
drush generate form-config

# Create a block
drush generate plugin-block

# Enable the module
drush en -y example_block
```

# Review and manual tweaks
- Enable the module.

- example_block.info.yml
  - Add configure: example_block.settings

-

### Building an Example Filter (example_filter) module.

```
cd ~/Sites/drupal_webform

# Generate the Example Filter module.
drush generate module-info

# Generate the Example Filter plugin.
drush generate plugin-filter

# Enable the Example Filter module.
drush en -y example_filter

# Add the Example Filter to the basic HTML text format.
# Make sure the <b> tags to allowed HTML.
open http://localhost/wf/admin/config/content/formats/manage/basic_html?destination=/wf/admin/config/content/formats

# Test the Example Filter (enter foo) to new node using the basic HTML text format.
open http://localhost/wf/node/add/page
```

### Testing generator.

```
drush generate --directory=~/Sites/drupal_webform/web/modules/sandbox/drupal_code_generator_example module-info
```

### Generator help.

```
drush generate

Drush generate 10.6.2

Run `drush generate [command]` and answer a few questions in order to write starter code to your project.

Available commands:
_global:
composer (composer.json)                                       Generates a composer.json file
controller                                                     Generates a controller
field                                                          Generates a field
hook                                                           Generates a hook
install-file                                                   Generates an install file
javascript                                                     Generates Drupal 8 JavaScript file
layout                                                         Generates a layout
migration                                                      Generates the yml and PHP class for a Migration
phpstorm-metadata                                              Generates PhpStorm metadata
project                                                        Generates a composer project
render-element                                                 Generates Drupal 8 render element
settings-local (settings.local.php)                            Generates Drupal 8 settings.local.php file
template                                                       Generates a template
drush:
drush-alias-file (daf)                                         Generates a Drush site alias file.
drush-command-file (dcf)                                       Generates a Drush command file.
form:
form-config (config-form)                                      Generates a configuration form
form-confirm (confirm-form)                                    Generates a confirmation form
form-simple                                                    Generates simple form
module:
module-configuration-entity (configuration-entity)             Generates configuration entity module
module-content-entity (content-entity)                         Generates content entity module
module-file                                                    Generates a module file
module-standard (module)                                       Generates standard Drupal 8 module
plugin:
plugin-action (action)                                         Generates action plugin
plugin-block (block)                                           Generates block plugin
plugin-ckeditor (ckeditor)                                     Generates CKEditor plugin
plugin-condition (condition)                                   Generates condition plugin
plugin-constraint (constraint)                                 Generates constraint plugin
plugin-entity-reference-selection (entity-reference-selection) Generates entity reference selection plugin
plugin-field-formatter (field-formatter)                       Generates field formatter plugin
plugin-field-type (field-type)                                 Generates field type plugin
plugin-field-widget (field-widget)                             Generates field widget plugin
plugin-filter (filter)                                         Generates filter plugin
plugin-manager                                                 Generates plugin manager
plugin-menu-link (menu-link)                                   Generates menu-link plugin
plugin-migrate-destination (migrate-destination)               Generates migrate destination plugin
plugin-migrate-process (migrate-process)                       Generates migrate process plugin
plugin-migrate-source (migrate-source)                         Generates migrate source plugin
plugin-queue-worker (queue-worker)                             Generates queue worker plugin
plugin-rest-resource (rest-resource)                           Generates rest resource plugin
plugin-views-argument-default (views-argument-default)         Generates views default argument plugin
plugin-views-field (views-field)                               Generates views field plugin
plugin-views-style (views-style)                               Generates views style plugin
service:
service-access-checker (access-checker)                        Generates an access checker service
service-breadcrumb-builder (breadcrumb-builder)                Generates a breadcrumb builder service
service-cache-context (cache-context)                          Generates a cache context service
service-custom (custom-service)                                Generates a custom Drupal service
service-event-subscriber (event-subscriber)                    Generates an event subscriber
service-logger (logger)                                        Generates a logger service
service-middleware (middleware)                                Generates a middleware
service-param-converter (param-converter)                      Generates a param converter service
service-path-processor (path-processor)                        Generates a path processor service
service-provider                                               Generates a service provider
service-request-policy (request-policy)                        Generates a request policy service
service-response-policy (response-policy)                      Generates a response policy service
service-route-subscriber (route-subscriber)                    Generates a route subscriber
service-theme-negotiator (theme-negotiator)                    Generates a theme negotiator
service-twig-extension (twig-extension)                        Generates Twig extension service
service-uninstall-validator (uninstall-validator)              Generates a uninstall validator service
test:
test-browser (browser-test)                                    Generates a browser based test
test-kernel (kernel-test)                                      Generates a kernel based test
test-nightwatch (nightwatch-test)                              Generates a nightwatch test
test-unit (unit-test)                                          Generates a unit test
test-web (web-test)                                            Generates a web based test
test-webdriver (webdriver-test)                                Generates a test that supports JavaScript
theme:
theme                                                          Generates Drupal 8 theme
theme-file                                                     Generates a theme file
theme-settings                                                 Generates Drupal 8 theme-settings.php file
yml:
yml-breakpoints (breakpoints)                                  Generates a breakpoints yml file
yml-links-action (action-links)                                Generates a links.action yml file
yml-links-contextual (contextual-links)                        Generates links.contextual yml file
yml-links-menu (menu-links)                                    Generates a links.menu yml file
yml-links-task (task-links)                                    Generates a links.task yml file
yml-module-info (module-info)                                  Generates a module info yml file
yml-module-libraries (module-libraries)                        Generates module libraries yml file
yml-permissions (permissions)                                  Generates a permissions yml file
yml-routing (routing)                                          Generates a routing yml file
yml-services (services)                                        Generates a services yml file
yml-theme-info (theme-info)                                    Generates a theme info yml file
yml-theme-libraries (theme-libraries)                          Generates theme libraries yml file
```
