<?php

namespace Drupal\Tests\drupal_code_generator_example\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test description.
 *
 * @group drupal_code_generator_example
 */
class DrupalCodeGeneratorExampleTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  public static $modules = ['drupal_code_generator_example'];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    // Mock required services here.
  }

  /**
   * Test callback.
   */
  public function testSomething() {
    $result = $this->container->get('transliteration')->transliterate('Друпал');
    $this->assertEquals('Drupal', $result);
  }

}
