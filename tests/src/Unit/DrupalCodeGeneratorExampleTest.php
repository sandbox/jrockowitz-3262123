<?php

namespace Drupal\Tests\drupal_code_generator_example\Unit;

use Drupal\Tests\UnitTestCase;

/**
 * Test description.
 *
 * @group drupal_code_generator_example
 */
class DrupalCodeGeneratorExampleTest extends UnitTestCase {

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    // @TODO: Mock required classes here.
  }

  /**
   * Tests something.
   */
  public function testSomething() {
    $this->assertTrue(TRUE, 'This is TRUE!');
  }

}
