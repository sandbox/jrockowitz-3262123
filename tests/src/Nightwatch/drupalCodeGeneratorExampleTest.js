module.exports = {
  '@tags': ['drupal_code_generator_example'],
  before(browser) {
    browser.drupalInstall();
  },
  after(browser) {
    browser.drupalUninstall();
  },
  'Front page': browser => {
    browser
      .drupalRelativeURL('/')
      .waitForElementVisible('body', 1000)
      .assert.containsText('h1', 'Log in')
      .drupalLogAndEnd({onlyOnError: false});
  },
};
