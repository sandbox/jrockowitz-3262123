/**
 * @file
 * Drupal Code Generator Example behaviors.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Behavior description.
   */
  Drupal.behaviors.drupalCodeGeneratorExample = {
    attach: function (context, settings) {

      console.log('It works!');

    }
  };

} (jQuery, Drupal));
