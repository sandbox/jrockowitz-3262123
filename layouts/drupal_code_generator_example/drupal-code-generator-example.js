/**
 * @file
 * Custom behaviors for drupal code generator example layout.
 */

(function (Drupal) {

  'use strict';

  Drupal.behaviors.drupalCodeGeneratorExample = {
    attach: function (context, settings) {

      console.log('It works!');

    }
  };

} (Drupal));
