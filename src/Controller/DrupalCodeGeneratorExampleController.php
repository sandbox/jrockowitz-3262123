<?php

namespace Drupal\drupal_code_generator_example\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Drupal Code Generator Example routes.
 */
class DrupalCodeGeneratorExampleController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];

    return $build;
  }

}
