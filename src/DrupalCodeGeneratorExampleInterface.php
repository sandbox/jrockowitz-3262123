<?php

namespace Drupal\drupal_code_generator_example;

/**
 * Interface for drupal_code_generator_example plugins.
 */
interface DrupalCodeGeneratorExampleInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label();

}
