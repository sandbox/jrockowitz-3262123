<?php

namespace Drupal\drupal_code_generator_example;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Defines a service provider for the Drupal code generator example module.
 */
class DrupalCodeGeneratorExampleServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $container->register('drupal_code_generator_example.subscriber', 'Drupal\drupal_code_generator_example\EventSubscriber\DrupalCodeGeneratorExampleEventSubscriber')
      ->addTag('event_subscriber')
      ->addArgument(new Reference('messenger'));
  }

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container) {
    $modules = $container->getParameter('container.modules');
    if (isset($modules['dblog'])) {
      // Override default DB logger to exclude some unwanted log messages.
      $container->getDefinition('logger.dblog')
        ->setClass('\Drupal\dblog\Logger\DbLog');
    }
  }

}
