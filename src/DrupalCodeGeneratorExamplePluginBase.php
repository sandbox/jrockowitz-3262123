<?php

namespace Drupal\drupal_code_generator_example;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for drupal_code_generator_example plugins.
 */
abstract class DrupalCodeGeneratorExamplePluginBase extends PluginBase implements DrupalCodeGeneratorExampleInterface {

  /**
   * {@inheritdoc}
   */
  public function label() {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

}
