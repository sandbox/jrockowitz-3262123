<?php

namespace Drupal\drupal_code_generator_example\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Provides an Example constraint.
 *
 * @Constraint(
 *   id = "drupal_code_generator_example",
 *   label = @Translation("Example", context = "Validation"),
 * )
 *
 * @DCG
 * To apply this constraint on third party entity types implement either
 * hook_entity_base_field_info_alter() or hook_entity_bundle_field_info_alter().
 */
class DrupalCodeGeneratorExampleConstraint extends Constraint {

  public $errorMessage = 'The error message.';

}
