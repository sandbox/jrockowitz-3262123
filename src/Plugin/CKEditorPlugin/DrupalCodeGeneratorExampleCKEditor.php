<?php

namespace Drupal\drupal_code_generator_example\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "Example" plugin.
 *
 * @CKEditorPlugin(
 *   id = "drupal_code_generator_example",
 *   label = @Translation("Example"),
 *   module = "drupal_code_generator_example"
 * )
 */
class DrupalCodeGeneratorExampleCKEditor extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return drupal_get_path('module', 'drupal_code_generator_example') . '/js/plugins/drupal-code-generator-example/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    $module_path = drupal_get_path('module', 'drupal_code_generator_example');
    return [
      'drupal-code-generator-example' => [
        'label' => $this->t('Example'),
        'image' => $module_path . '/js/plugins/drupal-code-generator-example/icons/drupal-code-generator-example.png',
      ],
    ];
  }

}
