<?php

namespace Drupal\drupal_code_generator_example\Plugin\DrupalCodeGeneratorExample;

use Drupal\drupal_code_generator_example\DrupalCodeGeneratorExamplePluginBase;

/**
 * Plugin implementation of the drupal_code_generator_example.
 *
 * @DrupalCodeGeneratorExample(
 *   id = "foo",
 *   label = @Translation("Foo"),
 *   description = @Translation("Foo description.")
 * )
 */
class Foo extends DrupalCodeGeneratorExamplePluginBase {

}
