<?php

namespace Drupal\drupal_code_generator_example\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Defines 'drupal_code_generator_example' queue worker.
 *
 * @QueueWorker(
 *   id = "drupal_code_generator_example",
 *   title = @Translation("Example"),
 *   cron = {"time" = 60}
 * )
 */
class DrupalCodeGeneratorExampleQueueWorker extends QueueWorkerBase {

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    // @todo Process data here.
  }

}
