<?php

namespace Drupal\drupal_code_generator_example\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines drupal_code_generator_example annotation object.
 *
 * @Annotation
 */
class DrupalCodeGeneratorExample extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
