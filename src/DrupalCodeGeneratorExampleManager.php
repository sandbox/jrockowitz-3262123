<?php

namespace Drupal\drupal_code_generator_example;

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * DrupalCodeGeneratorExampleManager service.
 */
class DrupalCodeGeneratorExampleManager {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a DrupalCodeGeneratorExampleManager object.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The current route match.
   */
  public function __construct(RouteMatchInterface $route_match) {
    $this->routeMatch = $route_match;
  }

  /**
   * Method description.
   */
  public function doSomething() {
    // @DCG place your code here.
  }

}
